.PHONY: tests

start:
	docker-compose pull
	docker-compose up -d dev
	docker exec -t fc-web composer init-env
	docker exec -t fc-web composer install
	docker exec -t fc-web php artisan key:generate
	docker exec -t fc-web php artisan migrate --database mysql --path ./database/migrations/mysql
	docker exec -t fc-web php artisan migrate --database mongodb --path ./database/migrations/mongodb
	docker exec -t fc-web php artisan jwt:secret -n
	docker exec -t fc-web php artisan db:seed
	python -mwebbrowser http://localhost:8080

stop:
	docker-compose down

standards:
	docker exec -t fc-web php artisan ide-helper:generate
	docker exec -t fc-web php artisan ide-helper:models --write --ignore App\\Models\\Post
	docker exec -t fc-web vendor/bin/php-cs-fixer fix -v

exec:
	docker exec -it fc-web bash

tests:
	docker-compose up -d test
	docker exec -t fc-test /var/www/html/vendor/phpunit/phpunit/phpunit --configuration /var/www/html/phpunit.xml
	docker-compose stop test mysql-test mongo-test