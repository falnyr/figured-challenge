<?php

declare(strict_types=1);

use App\Helpers\Factory;
use Illuminate\Database\Seeder;

final class DevSeeder extends Seeder
{
    public function run(): void
    {
        $admin = Factory::createAdmin([
            'email' => 'admin@foo.bar',
            'password' => bcrypt('foobaz'),
        ]);

        Factory::createReader([
            'email' => 'reader@foo.bar',
            'password' => bcrypt('foobaz'),
        ]);

        Factory::createPost([
            'user_id' => $admin->id,
        ]);

        Factory::createPost([
            'user_id' => $admin->id,
        ]);

        Factory::createPost([
            'user_id' => $admin->id,
        ]);

        Factory::createPost([
            'user_id' => $admin->id,
        ]);

        Factory::createPost([
            'user_id' => $admin->id,
        ]);
    }
}
