<?php

declare(strict_types=1);

use App\Models\Post;
use App\Models\User;
use Faker\Generator as Faker;

/* @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define(Post::class, function (Faker $faker) {
    return [
        'user_id' => function () {
            return factory(User::class)->create()->id;
        },
        'title' => $faker->sentence,
        'slug' => $faker->unique()->slug,
        'content' => $faker->paragraphs(3, true),
    ];
});
