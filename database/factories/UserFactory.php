<?php

declare(strict_types=1);

use App\Enumerations\UserRole;
use App\Models\User;
use Faker\Generator as Faker;

/* @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $faker->password,
        'role_id' => $faker->randomElement(UserRole::members())->value(),
        'remember_token' => str_random(10),
    ];
});
