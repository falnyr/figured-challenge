<?php

declare(strict_types=1);

namespace Tests\Unit;

use App\Helpers\Factory;
use App\Models\User;
use Tests\TestCase;

class PostTest extends TestCase
{
    public function testHasUser()
    {
        $post = Factory::createPost();

        $this->assertInstanceOf(User::class, $post->user);
    }
}
