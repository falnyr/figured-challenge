<?php

declare(strict_types=1);

namespace Tests\Unit;

use App\Helpers\Factory;
use Tests\TestCase;

class UserTest extends TestCase
{
    public function testHasPosts()
    {
        $user = Factory::createUser();
        $post = Factory::createPost([
            'user_id' => $user->id,
        ]);

        $this->assertEquals($user->posts->first()->id, $post->id);
    }
}
