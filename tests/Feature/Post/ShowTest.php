<?php

declare(strict_types=1);

namespace Tests\Feature\Post;

use App\Helpers\Factory;
use Tests\TestCase;

class ShowTest extends TestCase
{
    public function testUnauthenticatedUserCannotAccessPost()
    {
        $post = Factory::createPost();

        $response = $this->getJson('/api/posts/'.$post->getRouteKey());
        $response
            ->assertStatus(401)
        ;
    }

    public function testAuthenticatedUserCanAccessShow()
    {
        $this->actingAs(Factory::createUser(), 'api');
        $post = Factory::createPost();

        $response = $this->getJson('/api/posts/'.$post->getRouteKey());
        $response
            ->assertStatus(200)
        ;
    }

    public function testCannotShowNonExistingPost()
    {
        $this->actingAs(Factory::createUser(), 'api');

        $response = $this->getJson('/api/posts/qux');
        $response->assertStatus(404);
    }

    public function testResponseContainsPostDetails()
    {
        $post = Factory::createPost();
        $this->actingAs(Factory::createUser(), 'api');

        $response = $this->getJson('/api/posts/'.$post->getRouteKey());
        $response
            ->assertStatus(200)
            ->assertSee($post->slug)
            ->assertSee($post->title)
            ->assertSeeText(json_encode($post->content))
        ;
    }
}
