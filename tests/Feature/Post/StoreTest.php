<?php

declare(strict_types=1);

namespace Tests\Feature\Post;

use App\Helpers\Factory;
use Tests\TestCase;

class StoreTest extends TestCase
{
    public function testCannotCreatePostAsUnauthenticatedUser()
    {
        $response = $this->postJson('/api/posts');
        $response->assertStatus(401);
    }

    public function testCannotCreatePostAsReaderUser()
    {
        $this->actingAs(Factory::createReader(), 'api');

        $response = $this->postJson('/api/posts');
        $response->assertStatus(403);
    }

    public function testCannotCreatePostWithEmptyContent()
    {
        $this->actingAs(Factory::createAdmin(), 'api');

        $response = $this->postJson('/api/posts', []);
        $response->assertStatus(422);
    }

    public function testCannotCreatePostWithDuplicateSlug()
    {
        $this->actingAs(Factory::createAdmin(), 'api');

        Factory::createPost([
            'slug' => 'foobar',
        ]);

        $response = $this->postJson('/api/posts', [
            'title' => 'Foo Bar',
            'slug' => 'foobar',
            'content' => 'Lorem ipsum',
        ]);
        $response->assertStatus(422);
    }

    public function testAdministratorCanCreatePost()
    {
        $this->actingAs(Factory::createAdmin(), 'api');

        $response = $this->postJson('/api/posts', [
            'title' => 'Foo Baz',
            'slug' => 'foobaz',
            'content' => 'Lorem ipsum',
        ]);
        $response->assertStatus(201);
    }
}
