<?php

declare(strict_types=1);

namespace Tests\Feature\Post;

use App\Helpers\Factory;
use Tests\TestCase;

class IndexTest extends TestCase
{
    public function testUnauthenticatedUserCannotAccessIndex()
    {
        $response = $this->getJson('/api/posts');
        $response
            ->assertStatus(401)
        ;
    }

    public function testAuthenticatedReaderCanAccessIndex()
    {
        $user = Factory::createReader();
        $this->actingAs($user, 'api');

        $response = $this->getJson('/api/posts');
        $response
            ->assertStatus(200)
        ;
    }

    public function testAuthenticatedAdminCanAccessIndex()
    {
        $user = Factory::createAdmin();
        $this->actingAs($user, 'api');

        $response = $this->getJson('/api/posts');
        $response
            ->assertStatus(200)
        ;
    }

    public function testResponseContainsPosts()
    {
        Factory::createPost(['title' => 'foobar']);
        Factory::createPost(['title' => 'bazqux']);

        $this->actingAs(Factory::createAdmin(), 'api');

        $response = $this->getJson('/api/posts');
        $response
            ->assertStatus(200)
            ->assertSee('foobar')
            ->assertSee('bazqux')
        ;
    }
}
