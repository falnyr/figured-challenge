<?php

declare(strict_types=1);

namespace Tests\Feature\Post;

use App\Helpers\Factory;
use Tests\TestCase;

class DestroyTest extends TestCase
{
    public function testUnauthenticatedUserCannotDeletePost()
    {
        $post = Factory::createPost();

        $response = $this->deleteJson('/api/posts/'.$post->getRouteKey());
        $response->assertStatus(401);
    }

    public function testReaderUserCannotDeletePost()
    {
        $post = Factory::createPost();
        $this->actingAs(Factory::createReader(), 'api');

        $response = $this->deleteJson('/api/posts/'.$post->getRouteKey());
        $response->assertStatus(403);
    }

    public function testCannotDeleteNonExistingPost()
    {
        $this->actingAs(Factory::createReader(), 'api');

        $response = $this->deleteJson('/api/posts/qux');
        $response->assertStatus(404);
    }

    public function testAdminUserCannotDeletePostOfAnotherAdmin()
    {
        $user = Factory::createAdmin();
        $post = Factory::createPost();
        $this->actingAs($user, 'api');

        $response = $this->deleteJson('/api/posts/'.$post->getRouteKey());
        $response->assertStatus(403);
    }

    public function testAdminUserCanDeletePost()
    {
        $user = Factory::createAdmin();
        $post = Factory::createPost(['user_id' => $user->id]);
        $this->actingAs($user, 'api');

        $response = $this->deleteJson('/api/posts/'.$post->getRouteKey());
        $response->assertStatus(204);
    }
}
