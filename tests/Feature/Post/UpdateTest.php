<?php

declare(strict_types=1);

namespace Tests\Feature\Post;

use App\Helpers\Factory;
use Tests\TestCase;

class UpdateTest extends TestCase
{
    public function testCannotUpdatePostWithoutBeingLoggedIn()
    {
        $post = Factory::createPost();

        $response = $this->putJson('/api/posts/'.$post->getRouteKey());
        $response->assertStatus(401);
    }

    public function testCannotUpdatePostAsReader()
    {
        $post = Factory::createPost();
        $this->actingAs(Factory::createReader(), 'api');

        $response = $this->putJson('/api/posts/'.$post->getRouteKey());
        $response->assertStatus(403);
    }

    public function testCannotUpdateNonExistingPost()
    {
        $this->actingAs(Factory::createAdmin(), 'api');

        $response = $this->putJson('/api/posts/qux');
        $response->assertStatus(404);
    }

    public function testCannotUpdatePostWithoutAnyDetails()
    {
        $user = Factory::createAdmin();
        $post = Factory::createPost(['user_id' => $user->id]);
        $this->actingAs($user, 'api');

        $response = $this->putJson('/api/posts/'.$post->getRouteKey());
        $response->assertStatus(422);
    }

    public function testCannotUpdatePostToDuplicateSlug()
    {
        $user = Factory::createAdmin();
        $post = Factory::createPost([
            'user_id' => $user->id,
        ]);
        Factory::createPost([
            'slug' => 'barbaz',
        ]);

        $this->actingAs($user, 'api');

        $response = $this->putJson('/api/posts/'.$post->getRouteKey(), [
            'slug' => 'barbaz',
        ]);
        $response->assertStatus(422);
    }

    public function testAdministatorCannotUpdatePostOfAnotherAdministrator()
    {
        $post = Factory::createPost([
            'title' => 'titlefoo',
            'content' => 'contentfoo',
            'slug' => 'bazqux',
        ]);
        $this->actingAs(Factory::createAdmin(), 'api');

        $response = $this->putJson('/api/posts/'.$post->getRouteKey(), [
            'slug' => 'quxquux',
            'title' => 'titlebar',
            'content' => 'contentbar',
        ]);
        $response->assertStatus(403);
    }

    public function testAdministratorCanUpdateHisPost()
    {
        $user = Factory::createAdmin();
        $post = Factory::createPost([
            'user_id' => $user->id,
        ]);
        $this->actingAs($user, 'api');

        $response = $this->putJson('/api/posts/'.$post->getRouteKey(), [
            'slug' => 'quxquux',
            'title' => 'titlebar',
            'content' => 'contentbar',
        ]);
        $response
            ->assertStatus(200)
            ->assertSee('quxquux')
            ->assertSee('titlebar')
            ->assertSee('contentbar')
        ;
    }
}
