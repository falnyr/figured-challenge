<?php

declare(strict_types=1);

namespace Tests\Feature\Auth;

use App\Helpers\Factory;
use Tests\TestCase;

class LoginTest extends TestCase
{
    public function testCannotLoginWithoutProvidingAnyDetails()
    {
        $response = $this->postJson('/api/auth/login');
        $response->assertStatus(422);
    }

    public function testCannotLoginUsingGetMethod()
    {
        $response = $this->getJson('/api/auth/login');
        $response->assertStatus(405);
    }

    public function testCannotLoginWithInvalidDetails()
    {
        Factory::createReader([
            'email' => 'foo@bar.baz',
            'password' => bcrypt('foobar'),
        ]);

        $response = $this->postJson('/api/auth/login', [
           'email' => 'foo@bar.baz',
           'password' => 'foobaz',
        ]);
        $response->assertStatus(401);
    }

    public function testCanLoginWithValidCredentials()
    {
        Factory::createReader([
            'email' => 'bar@baz.qux',
            'password' => bcrypt('foobar'),
        ]);

        $response = $this->postJson('/api/auth/login', [
            'email' => 'bar@baz.qux',
            'password' => 'foobar',
        ]);
        $response->assertStatus(204);

        $authorization = $response->headers->get('Authorization');
        $response2 = $this->getJson('/api/user', ['Authorization' => $authorization]);
        $response2
            ->assertStatus(200)
            ->assertSee('bar@baz.qux')
        ;
    }
}
