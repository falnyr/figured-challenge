<?php

declare(strict_types=1);

namespace Tests\Feature\Auth;

use App\Helpers\Factory;
use Tests\TestCase;

class UserTest extends TestCase
{
    public function testCannotObtainCurrentUserWithoutLoggingIn()
    {
        $response = $this->getJson('/api/user');
        $response->assertStatus(401);
    }

    public function testCanGetCurrentlyLoggedInUser()
    {
        $this->actingAs(Factory::createUser(['email' => 'foo@bar.qux']), 'api');

        $response = $this->getJson('/api/user');
        $response
            ->assertStatus(200)
            ->assertSee('foo@bar.qux')
        ;
    }
}
