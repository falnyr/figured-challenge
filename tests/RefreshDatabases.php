<?php

declare(strict_types=1);

namespace Tests;

use Illuminate\Contracts\Console\Kernel;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\RefreshDatabaseState;

trait RefreshDatabases
{
    use RefreshDatabase {
        refreshTestDatabase as protected refreshSingleTestDatabase;
    }

    protected function refreshTestDatabase()
    {
        if ( ! RefreshDatabaseState::$migrated) {
            $this->migrate('mysql');
            $this->migrate('mongodb');

            $this->app[Kernel::class]->setArtisan(null);
            RefreshDatabaseState::$migrated = true;
        }
    }

    protected function migrate(string $database): void
    {
        $parameters = [
            '--database' => $database,
            '--path' => './database/migrations/'.$database,
        ];
        if ($this->shouldDropViews()) {
            $parameters['--drop-views'] = true;
        }

        $this->artisan('migrate:fresh', $parameters);
    }
}
