# Figured Coding Challenge

Build a blog application where `users` can read `posts` and where an admin user can login and create, update and delete posts.

You should use the following as part of the project:
- Laravel 5.6
- PHP 7
- MongoDB for storing the blog posts 
- MySQL for users

### Prerequisites

- [Docker](https://www.docker.com/get-started)
- [Docker compose](https://github.com/docker/compose/releases) (Linux only)

### Usage
###### Start the development environment
```
make start
```
###### Stop the environment
```
make stop
```
###### Run the tests
```
make tests
```
###### Coding standards
```
make standards
```