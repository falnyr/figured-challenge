let path = require('path');
const { VueLoaderPlugin } = require('vue-loader');

module.exports = {
    plugins: [
        new VueLoaderPlugin()
    ],

    entry: './resources/assets/js/app.js',

    output: {
        path: path.resolve(__dirname, 'public/js'),
        filename: "app.js",
        publicPath: "./public",
    },

    resolve: {
        alias: {
            'vue$': 'vue/dist/vue.common.js'
        }
    },

    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader',
            },
        ],
    },
};