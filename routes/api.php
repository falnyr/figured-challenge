<?php

Route::group(['middleware' => 'guest'], function () {
    Route::post('/auth/login', 'AuthController@login');
});

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('/user', 'UserController@show');

    Route::get('/posts/{post}', 'PostController@show');
    Route::put('/posts/{post}', 'PostController@update');
    Route::delete('/posts/{post}', 'PostController@destroy');
    Route::get('/posts', 'PostController@index');
    Route::post('/posts', 'PostController@store');
});