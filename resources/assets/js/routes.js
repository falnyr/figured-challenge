import VueRouter from 'vue-router';

import Index from './components/Index.vue';
import Posts from './components/Posts/List.vue';
import AddPost from './components/Posts/Add.vue';
import ShowPost from './components/Posts/Show.vue';
import EditPost from './components/Posts/Edit.vue';

let routes = [
    {
        name: 'Index',
        path: '/',
        component: Index,
    },
    {
        name: 'Add Post',
        path: '/posts/add',
        component: AddPost,
    },
    {
        name: 'Posts',
        path: '/posts',
        component: Posts,
    },
    {
        name: 'Show Post',
        path: '/posts/:slug',
        component: ShowPost,
    },
    {
        name: 'Edit Post',
        path: '/posts/:slug/edit',
        component: EditPost,
    }
];

export default new VueRouter({
    routes,
    linkExactActiveClass: 'is-active',
});