import Vue from 'vue';
import Vuex from 'vuex';
import Auth from './modules/auth';

Vue.use(Vuex);

let store = new Vuex.Store({
    modules: {
        Auth
    },
    mutations: {
        initialiseStore(state) {
            // Check if the ID exists
            if(localStorage.getItem('store')) {
                // Replace the state object with the stored item
                this.replaceState(
                    Object.assign(state, JSON.parse(localStorage.getItem('store')))
                );
            }
        }
    },
});

store.subscribe((mutation, state) => {
    localStorage.setItem('store', JSON.stringify(state));
});

export default store;