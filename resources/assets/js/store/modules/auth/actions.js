import Auth from '../../../api/auth'
import VueRouter from '../../../routes'

export const actions = {
    login({commit}, { email, password }) {
        Auth.login({email, password})
            .then(
                response => {
                    commit('loginSuccess', response);
                    VueRouter.push({name: 'Posts'});
                },
                error => {
                    commit('loginFailure', error);
                }
            )
        ;
    },
    logout({commit}) {
        Auth.logout();
        commit('logout');
    }
};
