import UserAPI from '../../../api/user'

export const mutations = {
    loginSuccess(state, response) {
        state.token = response.headers['authorization'];
        state.loggedIn = true;

        UserAPI.get().then(
            response => {
                state.user = response.data;
            },
            error => {
                console.log(error.response.data);
            }
        )

    },
    loginFailure(state) {
        state.token = null;
        state.loggedIn = false;
        state.user = null;
    },
    logout(state) {
        state.token = null;
        state.loggedIn = false;
        state.user = null;
    }
};
