import axios from 'axios';
import store from "../store";

const baseEndpoint = '/api/user';

export default {
    get() {
        let token = store.state.Auth.token;

        return axios.get(`${baseEndpoint}`, {
            headers: {
                'authorization': token,
            }
        });
    },
};
