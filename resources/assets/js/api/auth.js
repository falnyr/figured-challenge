import axios from 'axios';

const baseEndpoint = '/api/auth';

export default {
  login(credentials) {
    return axios.post(`${baseEndpoint}/login`, credentials);
  },
  logout() {
    return axios.post(`${baseEndpoint}/logout`);
  },
};
