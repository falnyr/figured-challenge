import axios from 'axios';
import store from '../store/index';

const baseEndpoint = '/api/posts';

export default {
    list() {
        let token = store.state.Auth.token;

        return axios.get(`${baseEndpoint}`, {
            headers: {
                'authorization': token,
            }
        });
    },
    show(slug) {
        let token = store.state.Auth.token;

        return axios.get(`${baseEndpoint}/${slug}`, {
            headers: {
                'authorization': token,
            }
        });
    },
    create(post) {
        let token = store.state.Auth.token;

        return axios.post(`${baseEndpoint}`, post, {
            headers: {
                'authorization': token,
            }
        });
    },
    update(slug, post) {
        let token = store.state.Auth.token;

        return axios.put(`${baseEndpoint}/${slug}`, post, {
            headers: {
                'authorization': token,
            }
        });
    },
    delete(slug) {
        let token = store.state.Auth.token;

        return axios.delete(`${baseEndpoint}/${slug}`, {
            headers: {
                'authorization': token,
            }
        });
    },
};
