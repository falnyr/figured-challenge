<router-link to="/" exact>
    <a class="navbar-item">Home</a>
</router-link>
<router-link to="/posts" exact>
    <a class="navbar-item">Posts</a>
</router-link>
<router-link to="/posts/add" exact>
    <a class="navbar-item">New Post</a>
</router-link>