<nav class="navbar is-transparent">
    <div class="navbar-brand">
        <a class="navbar-item" href="{{ url('/') }}">
            <img src="https://bulma.io/images/bulma-logo.png" alt="Bulma: a modern CSS framework based on Flexbox" width="112" height="28">
        </a>
        <div class="navbar-burger burger" data-target="navbar-menu">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>

    <div id="navbar-menu" class="navbar-menu">
        <div class="navbar-start">
            @include('nav')
        </div>
    </div>
</nav>