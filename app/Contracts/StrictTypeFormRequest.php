<?php

declare(strict_types=1);

namespace App\Contracts;

interface StrictTypeFormRequest
{
    public function authorize(): bool;

    public function rules(): array;
}
