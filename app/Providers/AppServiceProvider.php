<?php

declare(strict_types=1);

namespace App\Providers;

use App\Http\Controllers\PostController;
use Illuminate\Database\Connectors\ConnectionFactory;
use Illuminate\Database\DatabaseManager;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\ServiceProvider;
use Jenssegers\Mongodb\Connection;

final class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     */
    public function boot()
    {
        Resource::withoutWrapping();
    }

    /**
     * Register any application services.
     */
    public function register()
    {
        $this->app->when(PostController::class)
            ->needs(Connection::class)
            ->give(function () {
                /** @var DatabaseManager $databaseManager */
                $databaseManager = app()->makeWith(DatabaseManager::class, [
                    'app' => $this->app,
                    'factory' => $this->app->make(ConnectionFactory::class),
                ]);

                return $databaseManager->connection('mongodb');
            });
    }
}
