<?php

declare(strict_types=1);

namespace App\Helpers;

use App\Enumerations\UserRole;
use App\Models\Post;
use App\Models\User;

abstract class Factory
{
    public static function createAdmin(array $attributes = []): User
    {
        $attributes = array_merge(
            [
                'role_id' => UserRole::ADMIN,
            ],
            $attributes
        );

        return static::createUser($attributes);
    }

    public static function createReader(array $attributes = []): User
    {
        $attributes = array_merge(
            [
                'role_id' => UserRole::READER,
            ],
            $attributes
        );

        return static::createUser($attributes);
    }

    public static function createUser(array $attributes = []): User
    {
        return static::create(User::class, $attributes);
    }

    public static function createPost(array $attributes = []): Post
    {
        return static::create(Post::class, $attributes);
    }

    private static function create($class, array $attributes = [])
    {
        return factory($class)->create($attributes);
    }
}
