<?php

declare(strict_types=1);

namespace App\Enumerations;

use Eloquent\Enumeration\AbstractEnumeration;

final class UserRole extends AbstractEnumeration
{
    const ADMIN = 0;
    const READER = 1;
}
