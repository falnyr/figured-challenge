<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\Post\DestroyRequest;
use App\Http\Requests\Post\ShowRequest;
use App\Http\Requests\Post\StoreRequest;
use App\Http\Requests\Post\UpdateRequest;
use App\Http\Resources\PostResource;
use App\Models\Post;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;

final class PostController extends Controller
{
    public function index(): AnonymousResourceCollection
    {
        $posts = Post::query()->with('user')->paginate();

        return PostResource::collection($posts);
    }

    public function show(ShowRequest $request): PostResource
    {
        $post = $request->post;
        $post->load('user');

        return new PostResource($post);
    }

    public function store(StoreRequest $request): PostResource
    {
        $attributes = $request->only(['title', 'slug', 'content']);

        $post = new Post(array_merge($attributes, ['user_id' => $request->user()->id]));
        $post->save();

        return new PostResource($post);
    }

    public function update(UpdateRequest $request): PostResource
    {
        $attributes = $request->only(['title', 'slug', 'content']);
        $post = $request->post;
        $post->update($attributes);

        return new PostResource($post);
    }

    /**
     * @param DestroyRequest $request
     *
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function destroy(DestroyRequest $request): JsonResponse
    {
        $request->post->delete();

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
