<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\Auth\LoginRequest;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Tymon\JWTAuth\JWTAuth;

final class AuthController extends Controller
{
    /**
     * @var JWTAuth
     */
    private $auth;

    public function __construct(JWTAuth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * @param LoginRequest $request
     *
     * @return JsonResponse
     *
     * @throws AuthenticationException
     */
    public function login(LoginRequest $request): JsonResponse
    {
        $credentials = $request->only(['email', 'password']);

        if (($token = $this->auth->attempt($credentials)) === false) {
            throw new AuthenticationException();
        }

        return new JsonResponse(null, Response::HTTP_NO_CONTENT, [
            'Authorization' => 'Bearer '.$token,
        ]);
    }
}
