<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\User\ShowRequest;
use App\Http\Resources\UserResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

final class UserController extends Controller
{
    public function show(ShowRequest $request): JsonResponse
    {
        $resource = new UserResource($request->user());

        return $resource->response()->setStatusCode(Response::HTTP_OK);
    }
}
