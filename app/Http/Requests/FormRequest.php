<?php

declare(strict_types=1);

namespace App\Http\Requests;

use App\Contracts\StrictTypeFormRequest;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest as BaseFormRequest;

/**
 * Class FormRequest.
 *
 * @method User user()
 */
abstract class FormRequest extends BaseFormRequest implements StrictTypeFormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [];
    }
}
