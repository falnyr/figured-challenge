<?php

declare(strict_types=1);

namespace App\Http\Requests\Post;

use App\Rules\UniqueSlug;

final class UpdateRequest extends PostAwareRequest
{
    public function authorize(): bool
    {
        return $this->user()->can('update', $this->post);
    }

    public function rules(): array
    {
        return [
            'title' => ['required', 'string', 'max:255'],
            'content' => ['required', 'string'],
            'slug' => ['required', 'string', 'max:255', new UniqueSlug($this->post)],
        ];
    }
}
