<?php

declare(strict_types=1);

namespace App\Http\Requests\Post;

use App\Http\Requests\FormRequest;
use App\Models\Post;

/**
 * Class PostAwareRequest.
 *
 * @property Post $post
 */
abstract class PostAwareRequest extends FormRequest
{
}
