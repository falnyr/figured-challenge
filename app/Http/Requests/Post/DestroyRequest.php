<?php

declare(strict_types=1);

namespace App\Http\Requests\Post;

final class DestroyRequest extends PostAwareRequest
{
    public function authorize(): bool
    {
        return $this->user()->can('delete', $this->post);
    }
}
