<?php

declare(strict_types=1);

namespace App\Http\Requests\Post;

use App\Http\Requests\FormRequest;
use App\Models\Post;

final class IndexRequest extends FormRequest
{
    public function authorize(): bool
    {
        return $this->user()->can('view', Post::class);
    }
}
