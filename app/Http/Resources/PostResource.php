<?php

declare(strict_types=1);

namespace App\Http\Resources;

use App\Models\Post;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

final class PostResource extends JsonResource
{
    public function toArray($request): array
    {
        /** @var Post $model */
        $model = $this->resource;

        return [
            'id' => $model->id,
            'title' => $model->title,
            'slug' => $model->slug,
            'content' => $model->content,
            'user' => new UserResource($this->whenLoaded('user')),
            'created_at' => $model->created_at->format(Carbon::RFC3339),
        ];
    }
}
