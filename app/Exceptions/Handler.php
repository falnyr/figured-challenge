<?php

declare(strict_types=1);

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

final class Handler extends ExceptionHandler
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param Exception                $e
     *
     * @return JsonResponse
     */
    public function render($request, Exception $e): JsonResponse
    {
        $json = [];
        switch (true) {
            case $e instanceof AuthenticationException:
            case $e instanceof UnauthorizedHttpException:
                $status = Response::HTTP_UNAUTHORIZED;
                $json['error'] = 'Unauthorized';
                break;
            case $e instanceof AuthorizationException:
                $status = Response::HTTP_FORBIDDEN;
                $json['error'] = 'Forbidden';
                break;
            case $e instanceof ModelNotFoundException:
            case $e instanceof NotFoundHttpException:
                $status = Response::HTTP_NOT_FOUND;
                $json['error'] = 'Not Found';
                break;
            case $e instanceof MethodNotAllowedHttpException:
                $status = Response::HTTP_METHOD_NOT_ALLOWED;
                $json['error'] = 'Method Not Allowed';
                break;
            case $e instanceof ValidationException:
                $status = Response::HTTP_UNPROCESSABLE_ENTITY;
                $json['error'] = $e->errors();
                break;
            default:
                $status = Response::HTTP_INTERNAL_SERVER_ERROR;
                $json['error'] = 'Internal Server Error';
                break;
        }

        if (getenv('APP_DEBUG') === 'true' || getenv('APP_ENV') === 'testing') {
            $json['hint'] = [get_class($e), $e->getMessage(), $e->getFile().':'.$e->getLine()];
            $json['exception'] = $e->getTrace();
        }

        return new JsonResponse($json, $status);
    }
}
