<?php

declare(strict_types=1);

namespace App\Rules;

use App\Models\Post;
use Illuminate\Contracts\Validation\Rule;

class UniqueSlug implements Rule
{
    /**
     * @var Post|null
     */
    private $post;

    public function __construct(?Post $post = null)
    {
        $this->post = $post;
    }

    public function passes($attribute, $value): bool
    {
        $post = Post::query()->where('slug', '=', $value)->first();

        // cannot use query exists() since it's behaving weirdly with mongo.
        if ($post instanceof Post) {
            $passes = $post->id === $this->post->id;
        } else {
            $passes = true;
        }

        return $passes;
    }

    public function message(): string
    {
        return 'The slug is already taken';
    }
}
