<?php

declare(strict_types=1);

namespace App\Policies;

use App\Enumerations\UserRole;
use App\Models\Post;
use App\Models\User;

final class PostPolicy extends Policy
{
    public function view(User $user): bool
    {
        return in_array($user->role_id, [UserRole::ADMIN, UserRole::READER]);
    }

    public function create(User $user): bool
    {
        return $user->role_id === UserRole::ADMIN;
    }

    public function update(User $user, Post $post): bool
    {
        return $user->role_id === UserRole::ADMIN && $post->user_id === $user->id;
    }

    public function delete(User $user, Post $post): bool
    {
        return $user->role_id === UserRole::ADMIN && $post->user_id === $user->id;
    }
}
